//Declaring Dependencies.
var casper = require('casper').create({
	viewportSize: {width: 1200, height: 200}
});
var fs = require('fs');

//Declaring Vars
var startUrl = "https://www.bankslyon.co.uk/all-diamond-rings";
var domain = "https://www.bankslyon.co.uk";
var productLinks = [], productObject = [], productLinksList = [], nextLink = [];
var count = 1, urlCount = 1;

var title = '', details = '', price = '', stockCode = '', metal = '', colour = '', clarity = '', cut = '', cert = '', weight = '', totalWeight = '',
	picture = '', imgNameArr = '';

function final(){
	//console.log("\nComplete. Now posting to API");
	/*Post Data to the SQL Server*/
	var jsonString = JSON.stringify(productObject, null, 4);
	fs.write('BanksLyonData.json', jsonString, 'w');
	console.log(jsonString);		
	//Exiting the running application
	casper.exit();
	console.log("Finished. BanksLyonData.json Created"); 
}

function crawlIndividuals(){
	if (productLinksList.length > 0){
		casper.open(productLinksList[0]).then(function(){
			console.log("URL: " + productLinksList[0]);

			this.waitForSelector("h1.title", function(){
				title = this.fetchText("h1.title").trim();
				details = this.fetchText("#tab-product-description > #page-intro-text > p").trim();
				price = this.fetchText("#product-price-wrapper > #product-price").replace("£","").replace(".00","").trim();
				picture = this.getElementAttribute('#photos > .vslider-wrapper > .vslider-image > span > img', 'src');
				stockCode = this.fetchText("#product-sku").replace("Ref: ","").trim();
				
				if(picture != "" && picture != null){
					console.log("\n\n\nPicture: " + picture + "\n\n\n");
					imgNameArr = picture.split("/");
				}else{
					imgNameArr = "/NOTCATCHING/NOTCATCHING/NOTCATCHING";
				}

				if(this.exists("#tab-product-features")){
					var itemTitle = '#tab-product-features > ul > li > span.title';
					var itemContents = '#tab-product-features > ul > li > span:nth-child(2)';
					var itemDetails = this.getElementInfo(itemTitle);
					var itemDetails1 = this.getElementInfo(itemContents);
					itemTitle = [];

					for(var i = 0; i < itemDetails.length; i++){
						var tblTitle = itemDetails[i].text.toLowerCase().trim();
						var tblContents = itemDetails1[i].text.trim();

						switch(tblTitle){
							case "product type":
								type = tblContents.replace(/\s/g, " ");
								break;

							case "stone shape":
								stone = tblContents.replace(/\s/g, " ");
								break;

							case "material (jewellery)":
								metal = tblContents.replace(/\s/g, " ");
								break;

							case "certificated":
								cert = tblContents.replace(/\s/g, " ").replace(" Certificated","");
								break;

							case "diamond colour":
								colour = tblContents.replace(/\s/g, " ");
								break;

							case "diamond clarity":
								clarity = tblContents.replace(/\s/g, " ");
								break;

							case "diamond weight":
								weight = tblContents.replace(/\s/g, " ").replace(" carats","");
								break;

							default:
							console.log('Not Catching - ' + tblTitle);
							break;
						}
					}
				}

				if(title != ''){
					var productDetails = {
						Title: title,
						Details: details,
						Retail: price,
						StockCode: stockCode,
						Metal: metal,
						Colour: colour,
						Clarity: clarity,
						Stone: cut,
						//Certificate: cert,
						//CentreWeight: weight,
						TotalWeight: totalWeight,
						PictureRef: imgNameArr[imgNameArr.length -1],
						PictureURL: picture,
						WebPageURL: productLinksList[0]
					};

					productObject.push(productDetails);
					console.log("\n\n" + urlCount + ".)");
					urlCount++;
					console.log(JSON.stringify(productDetails) + "\n\n");
				}
			});

			this.then(function(){
				count++;
				productLinksList.shift();
				crawlIndividuals();
			})
		});
	}else{
		final();
	}
}

function openLinks(){
	casper.open(startUrl).then(function(){
		console.log("URL: " + startUrl);

		//Allowing the webpage 10 seconds to load all product in.
		this.wait(10000, function(){

			productLinks = this.evaluate(function(){
				var elements = __utils__.findAll('#productlist-container > .productlist > .productlist-item > .productlist-item-details > .productlist-item-details-title > a');
				return elements.map(function(e){
					return e.getAttribute('href');
				});
			});
		});
	});

	casper.then(function(){
		var splitLinks = productLinks.toString().split(",");

		console.log("Length: " + splitLinks.length);

		for (var i = 0; i < splitLinks.length; i++) {
			productLinksList.push(splitLinks[i]);
			console.log("Links: " + splitLinks[i]);
		}
	});

	casper.then(function(){
		if(this.exists('.pagination > #bot-pager > .next > a')){
			var loop = this.fetchText(".pagination > #bot-pager > li:nth-child(11) > a");
			//console.log("loop " + loop.toString().replace("... ","").split(" ")[1]);
			loop = parseInt(loop.toString().replace("... ","").split(" ")[1]);
			console.log(loop);
		}

		for (var i = 0; i < loop-1; i++) {
			//console.log("\n\n Looping through each page and grabbing URLs")
			this.waitForSelector("#page",function(){

				this.click(".pagination> #bot-pager > .next > a");

				//Allowing the webpage 10 seconds to load all product in.
				this.wait(10000, function(){

					productLinks = this.evaluate(function(){
						var elements = __utils__.findAll('#productlist-container > .productlist > .productlist-item > .productlist-item-details > .productlist-item-details-title > a');
						return elements.map(function(e){
							return e.getAttribute('href');
						});
					});

					var splitLinks = productLinks.toString().split(",");

						console.log("Length: " + splitLinks.length);

					for (var i = 0; i < splitLinks.length; i++) {
						productLinksList.push(splitLinks[i]);
						console.log("Links: " + splitLinks[i]);
					}
				});
			});
		}
	});

	casper.then(function(){
		crawlIndividuals();
	});
}

casper.start(startUrl, function(){
	openLinks();
});

casper.run();