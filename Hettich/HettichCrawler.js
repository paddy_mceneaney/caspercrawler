var casper = require('casper').create({
	viewportSize: {width: 1200, height: 22}
});
var fs = require('fs');

var startUrl = "https://www.hettich.co.uk/jewellery/rings";
var domain = "https://www.hettich.co.uk";
var productLinks = [], productObject = [];
var count = 1;

var title = '', details = '', price = '', stockCode = '', metal = '', colour = '', clarity = '', cut = '', cert = '', weight = '', totalWeight = '',
	picture = '', imgNameArr = '';

function final(){
	//console.log("\nComplete. Now posting to API");
	/*Post Data to the SQL Server*/
	var jsonString = JSON.stringify(productObject, null, 4);
	fs.write('HettichData.json', jsonString, 'w');
	console.log(jsonString);		
	//Exiting the running application
	casper.exit();
	console.log("Finished. HettichData.json Created");
}

function crawlIndividuals(){
	console.log("Product Links: " + productLinks.length);

	if(productLinks.length > 0){
		console.log(count + ".) " + domain+productLinks[0]);

		casper.open(domain+productLinks[0]).then(function(){
			title = this.fetchText("#innerboxbg > #homecontent > h1").trim();
			details = this.fetchText("#innerboxbg > #homecontent > p");
			price = this.fetchText("#innerboxbg > #homecontent > .prodprice").replace("£","");
			picture = this.getElementAttribute("#productsslideshow > .pika-image > a > img", "src");

		});
	}else{
		final();
	}
}

function openLinks(){
	casper.open(startUrl).then(function(){

		console.log(startUrl);

		//Allowing the webpage 10 seconds to load all product in
		this.wait(10000, function(){
			productLinks = this.evaluate(function(){
				var elements = __utils__.findAll('#products > .li > a');
				return elements.map(function(e){
					return e.getAttribute('href');
				});
			});
		});
	});

	casper.then(function(){
		crawlIndividuals();
	});
}

casper.start(startUrl, function(){
	openLinks();
});

casper.run();