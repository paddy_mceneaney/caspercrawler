//Declaring Dependencies.
var casper = require('casper').create({
	viewportSize: {width: 1200, height: 200}
});
var fs = require('fs');

//Declaring Vars
var startUrl = "https://www.keanes.ie/diamond-rings/";
var domain = "https://www.keanes.ie";
var productLinks = [], productObject = [], productLinksList = [], nextLink = [];
var count = 1, urlCount = 1, loopPage = 1;
var pageCount = "", splitLinks = "";

var title = '', details = '', price = '', stockCode = '', metal = '', colour = '', clarity = '', cut = '', cert = '', weight = '', totalWeight = '',
	picture = '', imgNameArr = '';

function final(){
	console.log("Last Stage");
	//console.log("\nComplete. Now posting to API");
	/*Post Data to the SQL Server*/
	var jsonString = JSON.stringify(productObject, null, 4);
	fs.write('KeanesData.json', jsonString, 'w');
	console.log(jsonString);		
	//Exiting the running application
	casper.exit();
	console.log("Finished. KeanesData.json Created"); 
}

function crawlIndividuals(){
	if(splitLinks.length > 0){
		console.log("Inside Crawl Individuals");
		console.log(splitLinks.length);

		casper.open(splitLinks[0]).then(function(){
			console.log("URL: " + splitLinks[0]);

			this.waitForSelector("h1#pageTitle", function(){
				title = this.fetchText("h1#pageTitle").replace(/\s/g, " ").replace("         Speak To An Expert","").trim();
				details = this.fetchText("p#productSummary").trim();
				price = this.fetchText("div.elements-container > div.price > p.price > .price-value").replace("€","").replace("Ôé¼","").replace(",","").replace(".00","").trim();
				picture = this.getElementAttribute("ul.slides > li#productSlide_1 img#productImage_1", "src");
				stockCode = this.fetchText("#productPosCode").replace("Keanes Code: ","").trim();
				imgNameArr = picture.split("/");

				if(title != ''){
					var productDetails = {
						Title: title,
						Details: details,
						Retail: price,
						StockCode: stockCode,
						//Metal: metal,
						//Colour: colour,
						//Clarity: clarity,
						//Stone: cut,
						//Certificate: cert,
						//CentreWeight: weight,
						//TotalWeight: totalWeight,
						PictureRef: imgNameArr[imgNameArr.length -1],
						PictureURL: picture,
						WebPageURL: splitLinks[0]
					};

					productObject.push(productDetails);
					console.log("\n\n" + urlCount + ".)");
					urlCount++;
					console.log(JSON.stringify(productDetails) + "\n\n");
				}
			});

			this.then(function(){
				splitLinks.shift();
				crawlIndividuals();
			});
		});
	}else{
		final();
	}
}

function getProducts(){
//	if(loopPage <= 1){
	if(loopPage <= parseInt(pageCount)){
		console.log("\n\nPage: " + loopPage);
		casper.wait(2000, function(){
			productLinks = this.evaluate(function(){
				var elements = __utils__.findAll("ul#productsListing > li > div.image > a");
				return elements.map(function(e){
					return e.getAttribute('href');
				});
			});

			productLinksList.push(productLinks.toString().split(","));

		});//End wait

		casper.then(function(){
			console.log("productLinks: " + productLinks);
	 		this.click("#nextPageButton-Top");
		});

		casper.then(function(){
			loopPage++;
			getProducts();
		});

	}else{
		console.log("\n\nFinished Looping Pages");
		splitLinks = productLinksList.toString().split(",");
		crawlIndividuals();
	}
}

function openLinks(){
	casper.open(startUrl).then(function(){

		this.wait(10000, function(){
			pageCount = this.fetchText("#pageCountVal-Bottom");
		});

		this.then(function(){
			getProducts();
		});
	});
}

casper.start(startUrl, function(){
	openLinks();
});

casper.run();