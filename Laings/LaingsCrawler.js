//Declaring Dependencies.
var casper = require('casper').create({
	viewportSize: {width: 1200, height: 200}
});
var fs = require('fs');

//var startUrl = "https://www.laingsuk.com/jewellery/rings/f/solitaires-cluster_rings-three_stone_rings-loose_stones/?limit=all&ignore=true";
var startUrl = "https://www.laingsuk.com/engagement?limit=all";
var domain = "https://www.laingsuk.com";
var productLinks = [], productObject = [];
var count = 1;

var title = '', details = '', price = '', stockCode = '', metal = '', colour = '', clarity = '', cut = '', cert = '', weight = '', totalWeight = '',
	picture = '', imgNameArr = '';

function final(){
	//console.log("\nComplete. Now posting to API");
	/*Post Data to the SQL Server*/
	var jsonString = JSON.stringify(productObject, null, 4);
	fs.write('LaingsData.json', jsonString, 'w');
	console.log(jsonString);		
	//Exiting the running application
	casper.exit();
	console.log("Finished. LaingsData.json Created"); 
}

function crawlIndividuals(){
	if(productLinks.length > 0){
		console.log(count + ".) " + productLinks[0]);

		casper.open(productLinks[0]).then(function(){

			if(this.exists("#collapseSpecification")){
				this.then(function(){
					title = this.fetchText('.product-title').trim();
					details = this.fetchText('.product-details > form > .description').trim();
					price = this.fetchText('.price-box > .regular-price > .price').replace("£","").replace(",","").replace(".00","").trim();
					
					if(this.exists('.MagicToolboxSelectorsContainer')){
						picture = this.getElementAttribute('.MagicToolboxSelectorsContainer > a:first-of-type > img', 'src');
					}else{
						picture = this.getElementAttribute('.product-image > .MagicToolboxContainer > a.MagicZoom', 'href');
					}

					imgNameArr = picture.split('/');

					if(this.exists('#product-attribute-specs-table')){
						var itemTitle = '#collapseSpecification .data-table tbody tr th';
						var itemContents = '#collapseSpecification .data-table tbody tr td';
						var itemDetails = this.getElementsInfo(itemTitle);
						var itemDetails1 = this.getElementsInfo(itemContents);
						itemTitle = [];

						for(var i = 0; i < itemDetails.length; i++){
							var tblTitle = itemDetails[i].text.toLowerCase().trim();
							var tblContents = itemDetails1[i].text.trim();

							switch(tblTitle){
								case "product code": 
									stockCode = tblContents.replace(/\s/g, " ");
									break;
								case "precious metal":
									metal = tblContents.replace(/\s/g, " ");
									break;
								case "diamond colour":
									colour = tblContents.replace(/\s/g, " ");
									break;
								case "diamond clarity":
									clarity = tblContents.replace(/\s/g, " ");
									break;
								case "diamond carat":
									totalWeight = tblContents.replace(/\s/g, " ");
									break;
								case "diamond cut":
									cut = tblContents.replace(/\s/g, " ");
									break;
								case "brand":
									brand = tblContents.replace(/\s/g, " ");
							}
						}
					}

					if(title != ''){
						var productDetails = {
							Title: title,
							Details: details,
							Retail: price,
							StockCode: stockCode,
							Metal: metal,
							Colour: colour,
							Clarity: clarity,
							Stone: cut,
							Brand: brand,
							//Certificate: cert,
							//CentreWeight: weight,
							TotalWeight: totalWeight,
							PictureRef: imgNameArr[imgNameArr.length -1],
							PictureURL: picture,
							WebPageURL: productLinks[0]
						};

						productObject.push(productDetails);
						console.log(JSON.stringify(productDetails) + "\n\n");
					}
				});

				this.then(function(){
					count++;
					productLinks.shift();
					crawlIndividuals();
				});
			}else{
				this.then(function(){
					count++;
					productLinks.shift();
					crawlIndividuals();
				});
			}			
		});

	}else{
		final();
	}
}

function openLinks(){
	casper.open(startUrl).then(function(){
		console.log(startUrl);

		//Allowing the webpage 10 seconds to load all product in
		this.wait(360000, function(){
			
			productLinks = this.evaluate(function(){
				var elements = __utils__.findAll('.card-container > .card > a.info-container');
				return elements.map(function(e){
					return e.getAttribute('href');
				})
			});
		});
	});

	casper.then(function(){
		crawlIndividuals();
	});
}

casper.start(startUrl, function(){
	openLinks();
});

casper.run();