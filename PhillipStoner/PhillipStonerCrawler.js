//Declaring Dependencies.
var casper = require('casper').create({
	viewportSize: {width: 1200, height: 200}
});
var fs = require('fs');

//Declaring Vars
var startUrl = "https://www.phillipstoner.com/rings/engagement-rings/#";
var domain = "https://www.phillipstoner.com";
var productLinks = [], productObject = [], productLinksList = [], nextLink = [];
var count = 1, pageCount = 2, loopCount = 1;
var splitLinks;

var title = '', details = '', price = '', stockCode = '', metal = '', colour = '', clarity = '', cut = '', cert = '', weight = '', totalWeight = '',
	picture = '', imgNameArr = '';

function final(){
	//console.log("\nComplete. Now posting to API");
	/*Post Data to the SQL Server*/
	var jsonString = JSON.stringify(productObject, null, 4);
	fs.write('PhillipStonerData.json', jsonString, 'w');
	console.log(jsonString);		
	//Exiting the running application
	casper.exit();
	console.log("Finished. PhillipStonerData.json Created"); 
}

function crawlIndividuals(){
	if (productLinksList.length > 0) {
		casper.open(productLinksList[0]).then(function(){
			console.log("Individual URL: " + productLinksList[0]);

			this.waitForSelector("#pb-left-column", function(){
				title = this.fetchText("#pb-left-column > h1").trim();
				details = this.fetchText(".short_description_block > ul > li").trim();
				price = this.fetchText(".price:first-of-type > .our_price_display:first-of-type > .our_price_display1").replace(",","").replace(".00","").replace("┬ú","").replace("£","").trim();
				picture = this.getElementAttribute("#image-block > #view_full_size > img","src");
				stockCode = this.fetchText("#product_reference_code > #product_reference > .editable").trim();
				imgNameArr = picture.split("/");
				totalWeight = title.split(" ")[0].replace(" ct","");
				colour = this.fetchText("#product_details_accordion > .ui-accordion-content:first-of-type > p:nth-child(1)").replace("Colour: ","").trim();
				clarity = this.fetchText("#product_details_accordion > .ui-accordion-content:first-of-type > p:nth-child(2)").replace("Clarity: ","").trim();
				cert = this.fetchText("#product_details_accordion > .ui-accordion-content:first-of-type > p:nth-child(3)").replace("Certificate: ","").replace(" Certificate","").trim();


				if(title != ''){
					var productDetails = {
						Title: title,
						Details: details,
						Retail: price,
						StockCode: stockCode,
						//Metal: metal,
						Colour: colour,
						Clarity: clarity,
						//Stone: cut,
						//Certificate: cert,
						//CentreWeight: weight,
						TotalWeight: totalWeight,
						PictureRef: imgNameArr[imgNameArr.length -1],
						PictureURL: picture,
						WebPageURL: productLinksList[0]
					};

					productObject.push(productDetails);
					console.log(JSON.stringify(productDetails) + "\n\n");
				}

			});

			this.then(function(){
				productLinksList.shift();
				crawlIndividuals();
			});
		});
	}else{
		final();
	}
}

function openLinks(){
	if(loopCount <= parseInt(loop)){
		console.log("Loop Count: " + loopCount);
		console.log("Pages to Loop: " + parseInt(loop));

		casper.open(startUrl).then(function(){
			console.log("URL: " + startUrl);

			this.wait(5000, function(){
				productLinks = this.evaluate(function(){
					var elements = __utils__.findAll('ul#product_list > li > .center_block > a');
					return elements.map(function(e){
						return e.getAttribute('href');
					});
				});
			});
		});

		casper.then(function(){
			console.log("\n\n*********************** INDIVIDUAL URLs ***********************");

			for (var i = 0; i < productLinks.length; i++) {
				console.log(count + ".) " + productLinks[i]);
				productLinksList.push(productLinks[i]);
				count++;
			}
		});

		casper.then(function(){	

			if(loopCount > 1){
				pageCount++;
				loopCount++;
			}else{
				loopCount++;
			}

			startUrl = "https://www.phillipstoner.com/rings/engagement-rings/#/page-" + pageCount.toString();
			console.log("\n\n" + startUrl);

		});

		casper.then(function(){
			openLinks();
		});
	}else{
		crawlIndividuals();
	}
}

function getPages(){
	casper.open(startUrl).then(function(){
		if(this.exists(".bottom ul.pagination > li:last-child > a")){
			loop = this.fetchText(".bottom ul.pagination > li:last-child > a");
		}
		console.log("Pages to loop: " + loop);
		openLinks();
	});
}

casper.start(startUrl, function(){
	getPages();
});

casper.run();