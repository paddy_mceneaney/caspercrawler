var casper = require('casper').create({
	viewportSize: {width: 1200, height: 22}
});
var fs = require('fs');

var startUrl = "https://www.rivolijewellers.co.uk/diamonds/engagement-rings";
var domain = "https://www.rivolijewellers.co.uk";
var productLinks = [], productObject = [];
var count = 1;

var title = '', details = '', price = '', stockCode = '', metal = '', colour = '', clarity = '', cut = '', cert = '', weight = '', totalWeight = '',
	picture = '', imgNameArr = '';


function final(){
	//console.log("\nComplete. Now posting to API");
	/*Post Data to the SQL Server*/
	var jsonString = JSON.stringify(productObject, null, 4);
	fs.write('RivoliData.json', jsonString, 'w');
	console.log(jsonString);		
	//Exiting the running application
	casper.exit();
	console.log("Finished. RivoliData.json Created");
}

function crawlIndividuals(){
	console.log("Product Links: " + productLinks.length);

	if(productLinks.length > 0){
		console.log(count + ".) " + domain+productLinks[0]);

		casper.open(domain+productLinks[0]).then(function(){
			title = this.fetchText(".product-details > h1").replace(/\s+/g, ' ').trim();
			details = this.fetchText(".product-details > p").replace(/\s+/g, ' ').trim();
			price = this.fetchText(".product-details > #price > .orange").replace("Approx UK Price: £ ","").trim();
			picture = this.getElementAttribute('.product-image-views > .large-product-thumbnail > img', 'src');

			if(picture != "" && picture != null){
				//console.log("\n\n\nPicture: " + domain + picture + "\n\n\n");
				picture = domain + picture;
				imgNameArr = picture.split('/');
			}else{
				imgNameArr = "/NOTCATCHING/NOTCATCHING/NOTCATCHING";
			}
			
			stockCode = this.fetchText(".product-details > #product-code").replace("Product Code:","").trim();

			if(this.exists('.product-details .table')){
				var itemTitle = "table.table tbody tr td:first-of-type";
				var itemContents = "table.table tbody tr td:nth-of-type(2)";
				var itemDetails = this.getElementsInfo(itemTitle);
				var itemDetails1 = this.getElementsInfo(itemContents);
				itemTitles = [];

				for (var i = 0; i < itemDetails.length; i++) {
					var tblTitle = itemDetails[i].text.toLowerCase().trim();
					var tblContents = itemDetails1[i].text.trim();

					switch(tblTitle){
						case "carat:":
							totalWeight = tblContents.replace(/\s/g, " ");
							break;

						case "diamond colour:":
							colour = tblContents.replace(/\s/g, " ");
							break;

						case "stone cut:":
							cut = tblContents.replace(/\s/g, " ");
							break;

						case "diamond clarity:":
							clarity = tblContents.replace(/\s/g, " ");
							break;

						case "certification:":
							cert = tblContents.replace(/\s/g, " ");
							break;

						case "colour:":
							break;

						case "material:":
							metal = tblContents.replace(/\s/g, " ");
							break;

						default:
							console.log("Not Catching - " + tblTitle);
							break;
					}
				}
			}
			
			console.log("\n\n\n\nTitle Length: " + title.length + "\nTitle: " + title + "\n\n\n");

			//details table
			if(title != '' && title != undefined && title.length < 150){

				var productDetails = {
					Title: title,
					Details: details,
					Retail: price,
					StockCode: stockCode[stockCode.length -1],
					Metal: metal,
					Colour: colour,
					Clarity: clarity,
					// Stone: cut,
					Certificate: cert,
					// CentreWeight: weight,
					TotalWeight: totalWeight,
					PictureRef: imgNameArr[imgNameArr.length -1],
					PictureURL: picture,
					WebPageURL: domain+productLinks[0]
				}
					productObject.push(productDetails);
					//console.log(JSON.stringify(productDetails) + "\n\n");
			}

			this.then(function(){
				count++;
				productLinks.shift();

				crawlIndividuals();
			});
		});
	}else{
		final();
	}
}

function openLinks(){
	casper.open(startUrl).then(function(){

		console.log(startUrl);

		//Allowing the webpage 10 seconds to load all product in
		this.wait(10000, function(){
			productLinks = this.evaluate(function(){
				var elements = __utils__.findAll('.product-list > .product-box > a');
				return elements.map(function(e){
					return e.getAttribute('href');
				});
			});
		});
	});

	casper.then(function(){
		crawlIndividuals();
	});
}

casper.start(startUrl, function(){
	openLinks();
});

casper.run();