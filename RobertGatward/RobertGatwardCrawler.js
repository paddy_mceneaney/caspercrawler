//Declaring Dependencies.
var casper = require('casper').create({
	viewportSize: {width: 1200, height: 200}
});
var fs = require('fs');

var startUrl = "https://www.robertgatwardjewellers.co.uk/engagement-rings?product_list_limit=108";
var domain = "https://www.robertgatwardjewellers.co.uk";
var nextLink = [], productLinks = [], productLinksList = [], productObject = [];
var count = 1;

var title = '', details = '', price = '', stockCode = '', metal = '', colour = '', clarity = '', cut = '', cert = '', weight = '', totalWeight = '',
	picture = '', imgNameArr = '';

function final(){
	//console.log("\nComplete. Now posting to API");
	/*Post Data to the SQL Server*/
	var jsonString = JSON.stringify(productObject, null, 4);
	fs.write('RobertGatwardData.json', jsonString, 'w');
	console.log(jsonString);		
	//Exiting the running application
	casper.exit();
	console.log("Finished. RobertGatwardData.json Created"); 
}

function crawlIndividuals(){
	console.log(productLinksList.length);
	if(productLinksList.length > 0){
		console.log(count + ".) " + productLinksList[0]);

		casper.open(productLinksList[0]).then(function(){

			this.waitForSelector(".page-title-wrapper .page-title", function(){
				title = this.fetchText(".page-title-wrapper .page-title").trim();
				details = this.fetchText(".product-info-main > .attribute > .value > p").trim();
				price = this.fetchText(".product-info-price > .price-box > span:first-of-type .price").replace(".50","").replace("┬ú","").replace("£","").replace(".00","").replace(",","").trim();
				picture = this.getElementAttribute('.fotorama__stage > .fotorama__stage__shaft > .fotorama__stage__frame > img', 'src');
				
				if(picture != "" && picture != null){
					console.log("\n\n\nPicture: " + picture + "\n\n\n");
					imgNameArr = picture.split("/");
				}else{
					imgNameArr = "/NOTCATCHING/NOTCATCHING/NOTCATCHING";
				}

				if(this.exists('.additional-attributes-wrapper > #product-attribute-specs-table')){
					var itemTitle = '#product-attribute-specs-table > tbody > tr > th';
					var itemContents = '#product-attribute-specs-table > tbody > tr > td';
					var itemDetails = this.getElementsInfo(itemTitle);
					var itemDetails1 = this.getElementsInfo(itemContents);
					itemTitle = [];

					for(var i = 0; i < itemDetails.length; i++){
						var tblTitle = itemDetails[i].text.toLowerCase().trim();
						var tblContents = itemDetails1[i].text.trim();

						switch(tblTitle){
							case "sku":
								stockCode = tblContents.replace(/\s/g, " ").replace("-","");
								break;
							case "metal/material":
								metal = tblContents.replace(/\s/g, " ");
								break;
							case "diamond colour":
								colour = tblContents.replace(/\s/g, " ");
								break;
							case "diamond clarity":
								clarity = tblContents.replace(/\s/g, " ");
								break;
							case "diamond cut":
								cut = tblContents.replace(/\s/g, " ");
								break;
							case "centre gemstone cut":
								cut = tblContents.replace(/\s/g, " ");
								break;
							case "total carat weight":
								totalWeight = tblContents.replace(/\s/g, " ");
								break; 
							case "diamond carat":
								totalWeight = tblContents.replace(/\s/g, " ");
								break;
						}
					}
				}

				if(title != ''){
					var productDetails = {
						Title: title,
						Details: details,
						Retail: price,
						StockCode: stockCode,
						Metal: metal,
						Colour: colour,
						Clarity: clarity,
						Stone: cut,
						// //Certificate: cert,
						// //CentreWeight: weight,
						TotalWeight: totalWeight,
						PictureRef: imgNameArr[imgNameArr.length -1],
						PictureURL: picture,
						WebPageURL: productLinksList[0]
					};

					productObject.push(productDetails);
					console.log(JSON.stringify(productDetails) + "\n\n");
				}
			});

			this.then(function(){
				count++;
				productLinksList.shift();
				crawlIndividuals();
			});
		});

	}else{
		final();
	}
}

function openLinks(){
	casper.open(startUrl).then(function(){
		console.log(startUrl);
		console.log("Product Links Length: " + productLinks.length);
		//Allowing the webpage 10 seconds to load all product in
		this.wait(10000, function(){
			console.log("Adding Individual Links");
			productLinks = this.evaluate(function(){
				var elements = __utils__.findAll('.products > .product-items > .product-item > .product-item-info > a');
				return elements.map(function(e){
					return e.getAttribute('href');
				});
			});
		});

		this.then(function(){

			for (var i = 0; i < productLinks.length; i++) {
				if(productLinks[i] != undefined){
					productLinksList.push(productLinks[i]);
				}				
			}

			if(this.exists(".niks-ajax-wrapper > .toolbar:first-of-type > .pager-container > .pages > .pages-items > .pages-item-next > a")){
				console.log("Exists");
				nextLink = this.evaluate(function(){
					var elements = __utils__.findAll('.niks-ajax-wrapper > .toolbar:first-of-type > .pager-container > .pages > .pages-items > .pages-item-next > a');
					return elements.map(function(e){
						return e.getAttribute('href');
					})
				});

				startUrl = nextLink.toString();
				openLinks();
			}else{
				crawlIndividuals();
			}
		})
	});
}	

casper.start(startUrl, function(){
	openLinks();
});

casper.run();