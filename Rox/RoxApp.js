//Declaring Dependencies.
var casper = require('casper').create();
var fs = require('fs');

//Declaring the variables
var pgNum = 1;
var domain = "https://www.rox.co.uk/engagement-rings?limit=120&p=";
var startUrl = domain + pgNum, pageToCrawl;
var overallArray = [], lastCheckedArray = [], currentPageArray = [];
var stringifyOverallArray = '', stringifyLastCheckedArray = '', stringifyCurrentPageArray = '', finalLinks = '';
var productObject = [];


function crawlLinks(){
	if(finalLinks.length > 0){
		console.log("Links Countdown: " + finalLinks.length);
		pageToCrawl = finalLinks.shift();

		casper.open(pageToCrawl).then(function(){
			var title = '', details = '', price = '', metal = '', lab = '',
			centreWeight = '', totalWeight, colour = '', cut = '', clarity = '', cert = '', picture = '',
			stockCode = '', brand = '', type = '';

			//Getting the Title of the product
			title = this.fetchText('.product-name > h1.roxproductattribute_name').replace(/\s/g, " ").replace('/','').replace('é','e').replace('�','').trim();

			//Getting the Price of the product
			price = this.fetchText('#priceheader > #pricebit').replace(/\s/g, " ").replace('£','').replace('/','').replace('é','e').replace('�','').replace('┬ú','').trim();

			//Getting the Details of the product
			details = this.fetchText('.roxproductattribute_description').replace(/\s/g, " ").replace('/','').replace('é','e').replace('�','').trim();

			//Getting the Stockcode of the product
			stockCode = this.fetchText('.roxproductattribute_stockcode').trim();

			//Getting the Picture of the product
			picture = this.getElementAttribute('.product-img-box > .large-image > img', 'src');

			//Getting the Cut of the product
			cut = this.fetchText('.dginfo > .roxproductattribute_cuttype').trim();

			//Getting the Clarity of the product
			clarity = this.fetchText('.dginfo > .roxproductattribute_diamond_ring_clarity').replace(" Clarity","").trim();

			//Getting the Colour of the product
			colour = this.fetchText('.dginfo > .roxproductattribute_diamond_ring_colour').replace(" Colour","").trim();

			//Getting the Total Weight of the product
			totalWeight = this.fetchText('.dginfo > .roxproductattribute_diamond_ring_carat').replace(" Carat","").trim();
			
			//Getting the Cert of the product
			cert = this.fetchText('.dginfo > .roxproductattribute_diamondissuer').trim();

			console.log(pageToCrawl);
			
			if(title != ''){
				var imgNameArr = picture.split('/');
				var productDetails = {
					Title: title,
					Details: details,
					Retail: price.replace("┬ú","").replace('Ôé¼',''),
					StockCode: stockCode,
					Metal: metal,
					Colour: colour,
					Clarity: clarity,
					Stone: cut,
					Certificate: cert,
					CentreWeight: centreWeight,
					TotalWeight: totalWeight,
					PictureRef: imgNameArr[imgNameArr.length -1],
					PictureURL: picture,
					WebPageURL: pageToCrawl
				};
				productObject.push(productDetails);
				console.log(JSON.stringify(productDetails));
			}

			crawlLinks();

		});
	}else{
		
		console.log("\nComplete. Now posting to API");
		/*Post Data to the SQL Server*/
		var jsonString = JSON.stringify(productObject, null, 4);
		fs.write('RoxData.json', jsonString, 'w');
		console.log(jsonString);		
		//Exiting the running application
		console.log("\nExiting");
		casper.exit();

	}
}//End crawlLinks();

function openLinks(){

	casper.open(startUrl).then(function(){
		console.log("Opening URL: " + startUrl + "\n");

		currentPageArray = this.evaluate(function(){
			var elements = __utils__.findAll('.products-grid > .item > a');
			return elements.map(function(e){
				return e.getAttribute('href');
			});
		});

		stringifyCurrentPageArray = currentPageArray.toString();

		console.log(stringifyCurrentPageArray);

		if(stringifyCurrentPageArray == stringifyLastCheckedArray){
			console.log("Matches: stringifyCurrentPageArray == stringinfyLastCheckedArray: " + overallArray.length);

			stringifyOverallArray = overallArray.toString();

			finalLinks = stringifyOverallArray.split(",");

			crawlLinks();
		}else{
			console.log("\n\nDoesn't match - Looping Page: " + pgNum + "\n");
			
			overallArray.push(currentPageArray);
			
			lastCheckedArray = currentPageArray;

			stringifyLastCheckedArray = stringifyCurrentPageArray;

			startUrl = domain + pgNum++;
			openLinks();
		}

	});
}//openLinks();

casper.start(startUrl, function(){
	startUrl = domain + pgNum++;
	openLinks();
});

casper.run();