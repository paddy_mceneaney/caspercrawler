//Declaring Dependencies.
var casper = require('casper').create({
	viewportSize: {width: 1200, height: 200}
});
var fs = require('fs');

var startUrl = "https://www.pragnell.co.uk/products/engagement-rings#p100";
var domain = "https://www.pragnell.co.uk";
var productLinks = [], productObject = [];
var count = 1;

var title = '', details = '', price = '', stockCode = '', metal = '', colour = '', clarity = '', cut = '', cert = '', weight = '', totalWeight = '',
	picture = '', imgNameArr = '';

function final(){
	//console.log("\nComplete. Now posting to API");
	/*Post Data to the SQL Server*/
	var jsonString = JSON.stringify(productObject, null, 4);
	fs.write('TarrattData.json', jsonString, 'w');
	console.log(jsonString);		
	//Exiting the running application
	casper.exit();
	console.log("Finished. TarrattData.json Created");
}

function crawlIndividuals(){
	if(productLinks.length > 0){
		
		console.log(count + ".) " + domain + productLinks[0]);
		casper.open(domain+productLinks[0]).then(function(){

			this.waitForSelector(".default_specifications", function(){
				title = this.fetchText('.product_detail > .specifications > h1 > span').trim();
				details = this.fetchText('.wrapper .description_tab').trim();
				price = this.fetchText('.specifications > div > p > span:nth-child(2)').replace(",","").replace(" (inc. VAT)", "").trim();
				picture = this.getElementAttribute('.image > picture > img', 'src');


				if(picture != "" && picture != null){
					console.log("\n\n\nPicture: " + picture + "\n\n\n");
					picture = "https:" + picture;
					imgNameArr = picture.split('/');
				}else{
					imgNameArr = "/NOTCATCHING/NOTCATCHING/NOTCATCHING";
				}


				stockCode = productLinks[0].split('/');

				if(this.exists('table.default_specifications')){

					var itemTitle = 'table.default_specifications tbody tr td:first-of-type';
					var itemContents = 'table.default_specifications tbody tr td:nth-of-type(2)';
					var itemDetails = this.getElementsInfo(itemTitle);
					var itemDetails1 = this.getElementsInfo(itemContents);
					itemTitles = [];					

					for(var i = 0; i < itemDetails.length; i++){
						
						var tblTitle = itemDetails[i].text.toLowerCase().trim();
						var tblContents = itemDetails1[i].text.trim();

						switch(tblTitle){
							case "material":
								metal = tblContents.replace(/\s/g, " ");
								break;

							case "principal cut":
								cut = tblContents.replace(/\s/g, " ");
								break;

							case "diamond colour":
								colour = tblContents.replace(/\s/g, " ");
								break;

							case "principal diamond clarity":
								clarity = tblContents.replace(/\s/g, " ");
								break;

							case "principal stone weight":
								weight = tblContents.replace(/\s/g, " ");
								break;

							case "total weight":
								totalWeight = tblContents.replace(/\s/g, " ");
								break;

							default:
								console.log('Not Catching - ' + tblTitle);
								break;
						}//End switch
					}//End for loop
				}//End if


				if(title != ''){

					var productDetails = {
						Title: title,
						Details: details,
						Retail: price,
						StockCode: stockCode[stockCode.length -1],
						Metal: metal,
						Colour: colour,
						Clarity: clarity,
						Stone: cut,
						//Certificate: cert,
						CentreWeight: weight,
						TotalWeight: totalWeight,
						PictureRef: imgNameArr[imgNameArr.length -1],
						PictureURL: picture,
						WebPageURL: domain+productLinks[0]
					};

					productObject.push(productDetails);
					console.log(JSON.stringify(productDetails) + "\n\n");
				}
			});			

			this.then(function(){
				count++;
				productLinks.shift();

				crawlIndividuals();
			});			
		});
		
	}else{
		final();
	}
}

function openLinks(){
	casper.open(startUrl).then(function(){
		
		console.log(startUrl);

		//Allowing the webpage 10 seconds to load all product in
		this.wait(10000, function(){
			productLinks = this.evaluate(function(){
				var elements = __utils__.findAll('.three-up-tall > .product > a');
				return elements.map(function(e){
					return e.getAttribute('href');
				});
			});
		});		
	});

	casper.then(function(){
		crawlIndividuals();
	});
}

casper.start(startUrl, function(){
	openLinks();
});

casper.run();