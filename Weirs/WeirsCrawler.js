//Declaring Dependencies.
var casper = require('casper').create();
var fs = require('fs');

//Declaring the variables
var pgNum = 1;
var domain = "https://weirandsons.ie/weddings-and-engagements/engagement-rings.html?p=";
var startUrl = domain + pgNum, pageToCrawl;
var overallArray = [], lastCheckedArray = [], currentPageArray = [];
var stringifyOverallArray = '', stringifyLastCheckedArray = '', stringifyCurrentPageArray = '', finalLinks = '';
var productObject = [];

function crawlLinks(){	
	
	if(finalLinks.length > 0){
		console.log("Links Countdown: " + finalLinks.length);
		pageToCrawl = finalLinks.shift();

		casper.open(pageToCrawl).then(function(){
			var title = '', details = '', price = '', metal = '', lab = '',
			centreWeight = '', totalWeight, colour = '', cut = '', clarity = '', cert = '', picture = '',
			stockCode = '', brand = '', type = '';

			if(this.exists('#tab-additional > table.data-table')){

				var itemTitle = '#tab-additional > table.data-table tbody tr th';
				var itemContents = '#tab-additional > table.data-table tbody tr td';

				var titleCount = this.fetchText('#tab-additional > table.data-table tbody tr th').length;
				var contentsCount = this.fetchText('#tab-additional > table.data-table tbody tr td').length;

				if(titleCount > 0 && contentsCount > 0){
					var itemDetails = this.getElementsInfo(itemTitle);
					var itemDetails1 = this.getElementsInfo(itemContents);
					itemTitles = [];
					
					for(var i = 0; i < itemDetails.length; i++){
						
						var tblTitle = itemDetails[i].text.toLowerCase().trim();
						var tblContents = itemDetails1[i].text.trim();

						switch(tblTitle){
							case "brand":
								brand = tblContents.replace(/\s/g, " ");
								break;

							case "colour of central stone":
								colour = tblContents.replace(/\s/g, " ");
								break;

							case "centre stone weight":
								centreweight = tblContents.replace(/\s/g, " ");
								break;

							case "combined weight":
								totalWeight = tblContents.replace(/\s/g, " ");
								break;

							case "metal":
								metal = tblContents.replace(/\s/g, " ");
								break;

							case "type of cut":
								cut = tblContents.replace(/\s/g, " ");
								break;

							case "centre stone clarity":
								clarity = tblContents.replace(/\s/g, " ");
								break;

							default:
								console.log('Not Catching - ' + tblTitle);
								break;
						}//End switch
					}//End for loop
				}
				
			}else{
				console.log("table doesn't exist");
			}//End if

			//Getting the Title of the product
			title = this.fetchText('.product-name > h1:first-of-type').replace(/\s/g, " ").replace('/','').replace('é','e').replace('�','').trim();

			//Getting the Price of the product
			price = this.fetchText('.brand .price-box > .regular-price > span.price').replace(/\s/g, " ").replace('Ôé¼','').replace('£','').replace('/','').replace('é','e').replace('�','').replace('┬ú','').trim();

			//Getting the Details of the product
			details = this.fetchText('#tab-description > .std').replace(/\s/g, " ").replace('/','').replace('é','e').replace('�','').trim();

			//Getting the Stockcode of the product
			stockCode = this.fetchText('.product-name > h1:last-of-type').trim();
			//Getting the Image Reference of the product
			picture = this.getElementAttribute('.image > .colorbox > img', 'src');

			console.log(pageToCrawl);
			if(title != ''){
			var imgNameArr = picture.split('/');
			var productDetails = {
				Title: title,
				Details: details,
				Retail: price.replace("┬ú","").replace('Ôé¼',''),
				StockCode: stockCode,
				Metal: metal,
				Colour: colour,
				Clarity: clarity,
				Stone: cut,
				Certificate: cert,
				CentreWeight: centreWeight,
				TotalWeight: totalWeight,
				PictureRef: imgNameArr[imgNameArr.length -1],
				PictureURL: picture,
				WebPageURL: pageToCrawl
			};
			productObject.push(productDetails);
			console.log(JSON.stringify(productDetails));
		}

		crawlLinks();
		});
	}else{

		console.log("\nComplete. Now posting to API");
		/*Post Data to the SQL Server*/
		var jsonString = JSON.stringify(productObject, null, 4);
		fs.write('WeirAndSonsData.json', jsonString, 'w');
		console.log(jsonString);		
		//Exiting the running application
		console.log("\nExiting");
		casper.exit();
	}
}//End CrawlLinks

function openLinks(){

	casper.open(startUrl).then(function(){
		console.log("Opening URL: " + startUrl + "\n");

		currentPageArray = this.evaluate(function(){
			var elements = __utils__.findAll('div.product-item > a');
			return elements.map(function(e){
				return e.getAttribute('href');
			});
		});

		stringifyCurrentPageArray = currentPageArray.toString();

		console.log(stringifyCurrentPageArray);

		if(stringifyCurrentPageArray == stringifyLastCheckedArray){
			console.log("Matches: stringifyCurrentPageArray == stringinfyLastCheckedArray: " + overallArray.length);

			stringifyOverallArray = overallArray.toString();

			finalLinks = stringifyOverallArray.split(",");

			crawlLinks();
		}else{

			console.log("\n\nDoesn't match - Looping Page: " + pgNum + "\n");
			overallArray.push(currentPageArray);
			lastCheckedArray = currentPageArray;

			stringifyLastCheckedArray = stringifyCurrentPageArray;

			startUrl = domain + pgNum++;
			openLinks();
		}
	});
}

casper.start(startUrl, function(){
	startUrl = domain + pgNum++;
	openLinks();
});

casper.run();