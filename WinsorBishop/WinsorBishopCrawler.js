//Declaring Dependencies.
var casper = require('casper').create({
	viewportSize: {width: 1200, height: 200}
});
var fs = require('fs');

var startUrl = "https://www.winsorbishop.co.uk/engagement-rings/?limit=all";
var domain = "https://www.winsorbishop.co.uk";
var productLinks = [], productObject = [];
var count = 1;

var title = '', details = '', price = '', stockCode = '', metal = '', colour = '', clarity = '', cut = '', cert = '', weight = '', totalWeight = '',
	picture = '', imgNameArr = '', brand = '';

function final(){
	//console.log("\nComplete. Now posting to API");
	/*Post Data to the SQL Server*/
	var jsonString = JSON.stringify(productObject, null, 4);
	fs.write('WinsorBishopData.json', jsonString, 'w');
	console.log(jsonString);		
	//Exiting the running application
	casper.exit();
	console.log("Finished. WinsorBishopData.json Created"); 
}

function crawlIndividuals(){
	if(productLinks.length > 0){
		console.log(count + ".) " + productLinks[0]);

		casper.open(productLinks[0]).then(function(){

			this.waitForSelector("#product-attribute-specs-table", function(){
				title = this.fetchText(".product-view-top > .product-name > h1").trim();
				details = this.fetchText(".box-collateral-content > .std > p").trim();
				price = this.fetchText(".price-box > .regular-price > .price").replace("£","").replace(",","").replace(".00","").trim();
				picture = this.getElementAttribute(".product-image > #magnify > a > img#magnify-small","src");
				stockCode = this.fetchText('.em-sku-availability > p.sku').replace("CODE: ", "").trim();
				imgNameArr = picture.split("/");

				if(this.exists('#product-attribute-specs-table')){
					var itemTitle = '#product-attribute-specs-table > tbody > tr > th';
					var itemContents = '#product-attribute-specs-table > tbody > tr > td';
					var itemDetails = this.getElementsInfo(itemTitle);
					var itemDetails1 = this.getElementsInfo(itemContents);
					itemTitle = [];

					console.log("Exists: " + itemDetails.length);
					for (var i = 0; i < itemDetails.length; i++) {
						var tblTitle = itemDetails[i].text.toLowerCase().trim();
						var tblContents = itemDetails1[i].text.trim();

						switch(tblTitle){
							case "lab":
								cert = tblContents.replace(/\s/g, " ");
								break;
							case "brand":
								brand = tblContents.replace(/\s/g, " ");
								break;
							case "metal":
								metal = tblContents.replace(/\s/g, " ");
								break;
							case "color":
								colour = tblContents.replace(/\s/g, " ");
								break;
							case "clarity":
								clarity = tblContents.replace(/\s/g, " ");
								break;
							case "cut":
								cut = tblContents.replace(/\s/g, " ");
								break;
							case "weight":
								weight = tblContents.replace(/\s/g, " ");;
								break;
							default:
								console.log('Not Catching - ' + tblTitle);
								break;
						}
					}

				}else{
					console.log("Doesnt exist");
				}

				if(title != ''){
					var productDetails = {
						Title: title,
						Details: details,
						Retail: price,
						Brand: brand,
						StockCode: stockCode,
						Metal: metal,
						Colour: colour,
						Clarity: clarity,
						Stone: cut,
						Certificate: cert,
						CentreWeight: weight,
						// TotalWeight: totalWeight,
						PictureRef: imgNameArr[imgNameArr.length -1],
						PictureURL: picture,
						WebPageURL: productLinks[0]
					};

					productObject.push(productDetails);
					console.log(JSON.stringify(productDetails) + "\n\n");
				}
			});

			this.then(function(){
				count++;
				productLinks.shift();
				crawlIndividuals();
			});
		});
	}else{
		final();
	}
	
}

function openLinks(){
	
	casper.open(startUrl).then(function(){
		
		console.log(startUrl);

		//Allowing the webpage 10 seconds to load all products in.
		this.wait(10000, function(){
			productLinks = this.evaluate(function(){
				var elements = __utils__.findAll('.item > .product-item > .product-shop-top > a.product-image');
				return elements.map(function(e){
					return e.getAttribute('href');
				});
			});
		});
	});

	casper.then(function(){
		crawlIndividuals();
	});
}

casper.start(startUrl, function(){
	openLinks();
});

casper.run();